/*
********************************************
Services
********************************************
*/
(function() {
  'use strict';

  var services = angular.module('app.services',[]);

  /*
  Busca os Modelos dos Carros
  */
  services.service('CarModelsService',  [ '$http' , function($http) {
    /*
    Modelos e Marcas  extraidos do site da webmotors : http://www.webmotors.com.br
    Fiz um serviço em PHP que consome os dados do site da webmotors
    Marcas:  http://feopt.com.br/desafiofrontend/service/models.php
    Modelos: http://feopt.com.br/desafiofrontend/service/models.php?brand=CHEVROLET
    */
    this.getBrands = function(callback){
      var url = "http://feopt.com.br/desafiofrontend/service/models.php";
      $http({
          method: 'GET',
          url: url
      }).success(function(response) {
        return callback(response);
      });
    }

    this.getModels = function(brand , callback){
      var url = "http://feopt.com.br/desafiofrontend/service/models.php";
      $http({
          method: 'GET',
          url: url,
          params : { brand : brand }
      }).success(function(response) {
        return callback(response);
      });
    }

  }]);

  /*
  Caros
  */
  services.service('CarService',  [ function() {

      var initCars = [
        {
          "id" : "0",
          "fuel" : "Flex",
          "image" : null,
          "brand" : "VOLKSWAGEN",
          "model" : "GOL",
          "plate" : "FFF­5498",
          "value" : 20000
        },
        {
          "id" : "1",
          "fuel" : "Gasolina",
          "image" : null,
          "brand" : "VOLKSWAGEN",
          "model" : "FOX",
          "plate" : "FOX­4125",
          "value" : 20000
        },
        {
          "id" : "2",
          "fuel" : "Alcool",
          "image" :"http://lorempixel.com/output/transport-q-c-640-480-8.jpg",
          "brand" : "VOLKSWAGEN",
          "model" : "FUSCA",
          "plate" : "PAI­4121",
          "value" : 20000
        }
      ];
      var cars = [];
      var uid = 0;


      this.sync = function(callback){
        // Analisa se o browser suporta o localStorage
        if(window.localStorage!==undefined){
          //Sincronizar Carros
          if(localStorage.getItem('cars') == null  ){
            localStorage.setItem( 'cars', JSON.stringify(initCars) );
          }
          cars = JSON.parse( localStorage.getItem('cars') );
          //Atualiza o tamanho do array para salvar os proximos
          uid = cars.length;
          return callback;
        }else{
            alert('Para utilizar o sistema você precisa atualizar seu navegador.');
        }
      }

      this.getByPage = function(offset, limit) {
        return cars.slice(offset, offset+limit);
      }

      this.get = function(id) {
        for (var i = 0; i < cars.length; i++) {
          if (cars[i].id == id) {
              return cars[i];
          }
        }
      }

      this.save = function (car) {
        if (car.id == null) {
            car.id = uid++;
            cars.push(car);
        } else {
            for (var i = 0; i < cars.length; i++) {

                if (cars[i].id == car.id) {
                    cars[i] = car;

                }
            }
        }
        //Atualiza o LocalStorage
        localStorage.setItem('cars', JSON.stringify(cars));
      }

      this.delete = function (id) {
          for (var i = 0; i < cars.length; i++) {
            if (cars[i].id == id) {
              cars.splice(i, 1);
              //Atualiza o LocalStorage
              localStorage.setItem('cars', JSON.stringify(cars));
            }
          }
      }

      this.list = function () {
        return cars;
      }

      this.total = function () {
        return cars.length;
      }

  }]);

  /*
  Upload de Image
  */
  services.service('UploadfileService', function() {
      var uploadFlag = true;
      this.check = function(divId , callback){
          $("#"+divId).click();
          if(uploadFlag){
            this.bind(divId , callback);
          }
          document.body.onfocus = function(){
            if($("#"+divId).val().length == 0){
              callback ( {
                'status' : -1,
                'result' : ''
              });
            }
          }
      }

      this.bind = function(divId , callback){
        uploadFlag = false;
        $(document).on('change','#'+divId,function(e){
          var fileInput = document.getElementById(divId);
          var file = fileInput.files[0];
          var imageType = /image.*/;
          if ( file == undefined || file == 'undefined' || file == '' ) {
              callback ( {
                'status' : 0,
                'result' : 'Arquivo não permitido'
              });
          } else {
            if (file.type.match(imageType) && file != undefined ) {
              var reader = new FileReader();
              reader.onload = function(e) {
                if(e.target.result.length <= 102400){
                  callback ({
                    'status' : 1,
                    'result' : reader.result
                  });
                 } else {
                  callback ({
                    'status' : 0,
                    'result' : 'O arquivo selecionado é muito grande: tamanho máximo permitido é 200KB.'
                  });
                 }
              }
              reader.readAsDataURL(file);
            } else {
               callback ( {
                 'status' : 0,
                 'result' : 'Arquivo inválido'
               });
            }
          }

        });
      }
  });

  /*
  Funções Gerais
    - Formata a Moeda
  */
  services.service('UtilsService',   function() {
    this.formatCurrency = function(value, always_show_decimal){
      if (value == null) {
        return "";
      }
      if (typeof(value) == "number"){
        value = value.toString();
      }
      var tokens = value.split(".");
      if (tokens[0].length > 3) {
        var integer = tokens[0];
        var tmp = [];
        var i = integer.length - 3;
        while (true) {
          tmp.unshift(integer.substring(i, i+3));
          if (i > 3) {
            i -= 3;
          } else {
            tmp.unshift(integer.substring(0, i));
            break;
          }
        }
        tokens[0] = tmp.join(".");
      }
      if (tokens.length == 1) {
        if (always_show_decimal) {
          if( tokens[0] < 0 && tokens[0].charAt(1) == "." ){
            tokens[0] = tokens[0].replace(/\./g,' ')
          }
          return tokens[0] + ",00"
        } else {
          return tokens[0];
        }
      } else {
        if (tokens[1].length == 1 && always_show_decimal) {
          tokens[1] += "0";
        }
        if (tokens[1].length > 2) {
          tokens[1] = tokens[1].substring(0, 2);
        }
        return tokens.join(",") ;
      }
    }
  });

}());

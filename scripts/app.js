(function() {
  'use strict';

  var app = angular.module('app',['app.controllers', 'app.directives' , 'app.services' , 'app.filters' ]);

  //Melhorar o impacto visual quando está carregando a página
  document.addEventListener("DOMContentLoaded", function(event) {
    $('#loading').fadeOut(function(){
        $('#container').fadeIn();
        $('#container').removeClass('hide');
    });
  });

}());

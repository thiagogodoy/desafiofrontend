/*
********************************************
Filtros
********************************************
*/
(function() {
  'use strict';

  var filters = angular.module('app.filters',[]);

  /*
  Filtro para formatação de valores em real
  */
	filters.filter('real', ["UtilsService", function(UtilsService) {
  		return function(input) {
        if(input == undefined || input==''){
  			   return "";
         } else {
           return "R$ " + UtilsService.formatCurrency(input, true);
         }
  		};
  }]);

}());

/*
********************************************
Diretivas
********************************************
*/
(function() {
  'use strict';

  var directives = angular.module('app.directives',[]);
  /*
  Deixar apenas maiusculas
  */
  directives.directive('capitalize', function() {
     return {
       require: 'ngModel',
       link: function(scope, element, attrs, modelCtrl) {
          var capitalize = function(inputValue) {
             if(inputValue == undefined) inputValue = '';
             var capitalized = inputValue.toUpperCase();
             if(capitalized !== inputValue) {
                modelCtrl.$setViewValue(capitalized);
                modelCtrl.$render();
              }
              return capitalized;
           }
           modelCtrl.$parsers.push(capitalize);
           capitalize(scope[attrs.ngModel]);  // capitalize initial value
       }
     };
  });

  /*
  Verifica se a modal está habilitada para desabilitar / habilitar os checkboxes
  */
  directives.directive("modalShow", ['$rootScope' , function ($rootScope) {
    return {
        restrict: "A",
        link: function (scope, element, attrs) {
          element.bind("hide.bs.modal", function () {
            var checkboxes = document.getElementsByName('checkboxView');
            for (var i = 0, n = checkboxes.length; i < n; i++)   {
              checkboxes[i].checked = false;
            }
          });
        }
    };
  }]);

  /*
  Formata a Moeda // Peguei de um site mais não me consegui encontrar o link
  */
  directives.directive("real", ["UtilsService" , function(UtilsService) {
    return {
      restrict: 'A',
	    require: 'ngModel',
	    link: function(scope, elem, attr, ctrl) {

        ctrl.$parsers.unshift(function(value) {
          if (typeof(value) == "string") {
	            	value = value.replace(/\s/g, '');
	            	value = value.replace(/[^0-9.,]+/g, '');
            		value = value.replace(/\./g, "").replace(/(,)+/g, ".");
            		if (value.search(/[^0-9,.]/) > -1) {
            			ctrl.$setValidity('real', false);
            			return null;
            		}
	            	// Remove os numeros depois da virgula
					      var newValue = value.split(".");
	            	if( newValue[1] && newValue[1].length > 2 ){
	            		value = value.slice(0, -1);
	            	}
	            	//BUG quando insere uma letra, seta falso
	            	if(value.length == 0 ){
	            		ctrl.$setValidity('real', false);
	            	}
	            	ctrl.$setViewValue(value);
            	}
            	ctrl.$setValidity('real', true);
            	elem.val(UtilsService.formatCurrency(value, false)); // Formata valor exibido ao usuario
         	    return value;
            });
            ctrl.$formatters.push(function(value){
              if (value != '')
            	 return UtilsService.formatCurrency( value, true );
            })
        }
      };
   }]);

  /*
  Mensagens de Erros no formulario
  Código parcial do projeto : https://github.com/paulyoder/angular-bootstrap-show-errors
  */
  directives.directive('showErrors', [
    '$timeout', 'showErrorsConfig', '$interpolate', function($timeout, showErrorsConfig, $interpolate) {
      var getShowSuccess, getTrigger, linkFn;
      getTrigger = function(options) {
        var trigger;
        trigger = showErrorsConfig.trigger;
        if (options && (options.trigger != null)) {
          trigger = options.trigger;
        }
        return trigger;
      };
      getShowSuccess = function(options) {
        var showSuccess;
        showSuccess = showErrorsConfig.showSuccess;
        if (options && (options.showSuccess != null)) {
          showSuccess = options.showSuccess;
        }
        return showSuccess;
      };
      linkFn = function(scope, el, attrs, formCtrl) {
        var blurred, inputEl, inputName, inputNgEl, options, showSuccess, toggleClasses, trigger;
        blurred = false;
        options = scope.$eval(attrs.showErrors);
        showSuccess = getShowSuccess(options);
        trigger = getTrigger(options);
        inputEl = el[0].querySelector('.form-control[name]');
        inputNgEl = angular.element(inputEl);
        inputName = $interpolate(inputNgEl.attr('name') || '')(scope);
        if (!inputName) {
          throw "show-errors element has no child input elements with a 'name' attribute and a 'form-control' class";
        }
        inputNgEl.bind(trigger, function() {
          blurred = true;
          return toggleClasses(formCtrl[inputName].$invalid);
        });
        scope.$watch(function() {
          return formCtrl[inputName] && formCtrl[inputName].$invalid;
        }, function(invalid) {
          if (!blurred) {
            return;
          }
          return toggleClasses(invalid);
        });
        scope.$on('show-errors-check-validity', function() {
          return toggleClasses(formCtrl[inputName].$invalid);
        });
        scope.$on('show-errors-reset', function() {
          return $timeout(function() {
            el.removeClass('has-error');
            el.removeClass('has-success');
            return blurred = false;
          }, 0, false);
        });
        return toggleClasses = function(invalid) {
          el.toggleClass('has-error', invalid);
          if (showSuccess) {
            return el.toggleClass('has-success', !invalid);
          }
        };
      };
      return {
        restrict: 'A',
        require: '^form',
        compile: function(elem, attrs) {
          if (attrs['showErrors'].indexOf('skipFormGroupCheck') === -1) {
            if (!(elem.hasClass('form-group') || elem.hasClass('input-group'))) {
              throw "show-errors element does not have the 'form-group' or 'input-group' class";
            }
          }
          return linkFn;
        }
      };
    }
  ]);

  directives.provider('showErrorsConfig', function() {
    var _showSuccess, _trigger;
    _showSuccess = false;
    _trigger = 'blur';
    this.showSuccess = function(showSuccess) {
      return _showSuccess = showSuccess;
    };
    this.trigger = function(trigger) {
      return _trigger = trigger;
    };
    this.$get = function() {
      return {
        showSuccess: _showSuccess,
        trigger: _trigger
      };
    };
  });


}());

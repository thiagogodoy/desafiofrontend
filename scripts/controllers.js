/*
********************************************
Controller
********************************************
*/
(function() {
  'use strict';

  var controllers = angular.module('app.controllers',[]);

  controllers.controller('BaseController', ['$scope' ,  'CarService' , 'CarModelsService' , 'UploadfileService' ,  function($scope , CarService , CarModelsService , UploadfileService ) {

    /*
    Sincroniza dados. Grava no LocalStorage
    */
    CarService.sync();

    // Configurações iniciais
    $scope.updateVisibility = false; // Verifica se é update ou adição de novo carro
    $scope.search = ''; //Form de Busca
    $scope.carModels = ''; // Modelo dos carros
    $scope.loadingModels = false;
    $scope.carForm = {}; // Form de Carros
    $scope.cars = CarService.list(); // Carrega lista de carros

    //Busca a lista de modelos de carros
    CarModelsService.getBrands(function(data){
      $scope.carBrands = data;
    });

    /*
    Cria
    */
    $scope.create = function(){
      $scope.modalTitle = 'Adicionar Novo Carro';
      $scope.search = ''; //Limpa o campo de busca
      $scope.$broadcast('show-errors-reset'); //Limpa validação do formulário
      $scope.updateVisibility = false;
      $scope.newCar = {};
      $('#carModal').modal('show');
    };

    /*
    Edita
    */
    $scope.edit = function(id){
      $scope.modalTitle = 'Editar Carro';
      $scope.search = ''; //Limpa o campo de busca
      $scope.newCar = angular.copy(CarService.get(id));
      $scope.updateVisibility = true;
      $scope.$broadcast('show-errors-reset'); //Limpa validação do formulário
      //Carrega o modelo referente a marca
      $scope.getModels( $scope.newCar.brand , function(){
        $('#carModal').modal('show');
      });
    };

    /*
    Salva
    */
    $scope.save = function(){
      $scope.$broadcast('show-errors-check-validity');
      $scope.search = ''; //Limpa o campo de busca
      if ($scope.carForm.$valid) {
        CarService.save($scope.newCar);
        //Mensagem
        if($scope.updateVisibility){
          $scope.status('Carro editado com sucesso');
        } else {
          $scope.status('Carro adicionado com sucesso');
        }
        $scope.resetRange();
        $scope.newCar = {};
        $scope.$broadcast('show-errors-reset');
        $('#carModal').modal('hide');
      }
    };

    /*
    Deleta
    */
    $scope.delete = function(id){
      $scope.search = ''; //Limpa o campo de busca
      if (confirm("Você realmente deseja excluir este carro?") == true) {
          CarService.delete(id);
          if ($scope.newCar.id == id) $scope.newCar = {};
          $('#carModal').modal('hide');
          $scope.status('Carro apagado com sucesso');
          //Atualiza a paginação
          $scope.resetRange();
      }
    }

    /*
    Carregar Modelos
    */
    $scope.getModels = function(brand , callback){
      $scope.loadingModels = true;
      CarModelsService.getModels( brand , function(data){
        $scope.carModels = data;
        $scope.loadingModels = false;
      });
      if(callback){
        callback();
      }
    }

    /*
    Status da ação
    */
    $scope.status = function(message){
      $scope.statusMessage = message;
      $('#status').fadeIn();
      setTimeout(function(){
          $('#status').fadeOut();
        }, 3000
      );
    }

    /*
    Abrir modal com a image
    */
    $scope.openImageModal = function(src){
      $scope.modalImage = src;
      $('#imageModal').modal('show');
    }

    /*
    Remove Image
    */
    $scope.removeImage= function(){
      $scope.newCar.image = '';
    }

    /*
    Faz upload da image
    */
    $scope.getPictureFromDisk = function() {
      UploadfileService.check( "image" ,  function( out ) {
          if(out.status == 1){
            $scope.newCar.image = out.result;
          } else if(out.status == 0){
            alert(out.result);
          }
          if(!$scope.$$phase) {
            $scope.$apply();
          }
      });
    }

    /*
    Paginação
    */
    $scope.itemsPerPage = 5;
    $scope.currentPage = 0;
    $scope.range = function() {
      var rangeSize = 5;
      var ret = [];
      var start;

      start = $scope.currentPage;
      if ( start > $scope.pageCount()-rangeSize ) {
        start = $scope.pageCount()-rangeSize;
      }
      for (var i=start; i<start+rangeSize; i++) {
        if(i >=0 ){
          ret.push(i);
        }
      }
      return ret;
    };

    $scope.resetRange = function(){
      $scope.total = CarService.total();
      $scope.pagedItems = CarService.getByPage(0, $scope.itemsPerPage);
      $scope.setPage(0);
    }

    $scope.prevPage = function() {
      if ($scope.currentPage > 0) {
        $scope.currentPage--;
      }
    };

    $scope.prevPageDisabled = function() {
      return $scope.currentPage === 0 ? "disabled" : "";
    };

    $scope.nextPage = function() {
      if ($scope.currentPage < $scope.pageCount() - 1) {
        $scope.currentPage++;
      }
    };

    $scope.nextPageDisabled = function() {
      return $scope.currentPage === $scope.pageCount() - 1 ? "disabled" : "";
    };

    $scope.pageCount = function() {
      return Math.ceil($scope.total/$scope.itemsPerPage);
    };

    $scope.setPage = function(n) {
      if (n >= 0 && n < $scope.pageCount()) {
        $scope.currentPage = n;
      }
    };

    $scope.$watch("currentPage", function(newValue, oldValue) {
      $scope.pagedItems = CarService.getByPage(newValue*$scope.itemsPerPage, $scope.itemsPerPage);
      $scope.total = CarService.total();
    });


  }]);

}());

var gulp    = require('gulp'),
    connect = require('gulp-connect'),
    concat  = require("gulp-concat");
    sass    = require('gulp-sass'),
    uglify  = require('gulp-uglify');

    /*
    Trata o Javascript
    */
    gulp.task('compress', function() {
      return gulp.src('./scripts/*.js')
        .pipe(uglify())
        .pipe(concat("app.js"))
        .pipe(gulp.dest('./app/js/'))
        .pipe(connect.reload());
    });

    /*
    Sass
    */
    gulp.task('sass', function () {
      gulp.src('./styles/**/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(sass({outputStyle: 'compressed'}))
        .pipe(gulp.dest('./app/css/'))
        .pipe(connect.reload());
    });

    /*
    Servidor
    */
    gulp.task('connect', ['watch'] , function() {
      connect.server({
        root: 'app',
        livereload: true
      });
    });

    /*
    Live Reload no html
    */
    gulp.task('html', function () {
      gulp.src(['./app/*.html' , './app/templates/*.html'])
        .pipe(connect.reload());
    });

    /*
    Arquivos Modificados
    */
    gulp.task('watch', function () {
      gulp.watch(['./app/*.html' , './app/templates/*.html'], ['html']);
      gulp.watch('./styles/**/*.scss', ['sass']);
      gulp.watch('./scripts/*.js', ['compress']);
    });

    //Buid da Aplicação
    gulp.task('build', ['html' , 'sass' , 'compress']);

    // Roda o Servidor por padrão
    gulp.task('default', ['connect']);

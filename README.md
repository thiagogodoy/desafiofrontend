![ContaAzul](http://meuholerite.com.br/img/logos_clientes/img_clientes_logo_conta_azul.png)

# Desafio Front-End
Mini aplicação para controle de frotas.

------

# Instalar o projeto

> npm install

# Rodar o servidor

> gulp  ou  npm start


------
# Teste automatizados

Instalar o Protractor: https://angular.github.io/protractor/#/

> npm install -g protractor

> webdriver-manager update

## Para rodar os testes

Verificar a configuração do seleniumAddress:

> tests/conf.js - seleniumAddress: 'http://127.0.0.1:4444/wd/hub',

Inicializar o Webdriver:

> webdriver-manager start

Inicializar o servidor local:

> gulp

Rodar os teste na raiz do projeto:

> protractor tests/conf.js


Ou rodar via npm:

> npm test


------

# Tecnologias
AngularJs

Protractor

Gulp

Sass

Bootstrap


# Ferramentas

Desafio Online em: http://feopt.com.br/desafiofrontend

Repositório: http://bitbucket.org

Atom: https://atom.io/

Site para gerar a font com os icones: https://icomoon.io/app/#/select

Estou sem photoshop então utilizei: http://www.photoshoponline.com.br/editor/

Trello para organizar os desenvolvimento: https://trello.com/b/8LPwYZDU/desafio-front-end

describe('Teste Visual dos Elementos: ', function() {

  it('Verificar Elementos Visuais Desafio', function() {

    //Verificar se esta correto o local host
    browser.get('http://localhost:8080/');
    browser.waitForAngular();

    var divHeader = element(by.className('header')),
        divSearchForm = element(by.className('custom_form')),
        divButtonAdd = element(by.className('button--add')),
        divTableContent = element(by.className('custom_table')),
        divCarModal = element(by.id('carModal')),
        allCars = element.all(by.css('.custom_table tbody tr'));

    // Verifica se existe os elementos
    console.log("Header");
    expect(divHeader.isPresent()).toBe(true);
    console.log("Form de busca");
    expect(divSearchForm.isPresent()).toBe(true);
    console.log("Botão de adicionar");
    expect(divButtonAdd.isPresent()).toBe(true);
    console.log("Table de carros");
    expect(divTableContent.isPresent()).toBe(true);
    console.log("Verifica se tem os 3 caros iniciais");
    expect(allCars.count()).toBe(3);
  });
});
